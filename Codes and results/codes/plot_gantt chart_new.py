import pandas as pd
import numpy as np
import random
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import timedelta


###############################################################################
'''  read file and define parameters   '''
###############################################################################
'''read excel file'''
co_times = pd.read_excel(
        r'C:\Users\LiZh\Desktop\P&G plant data\changeover_times.xlsx')
metadata = pd.read_excel(
        r'C:\\Users\LiZh\Desktop\P&G plant data\metadata_2.xlsx')
packingline_dedication = pd.read_excel(
        r'C:\Users\LiZh\Desktop\P&G plant data\packing_line_dedication.xlsx')
packingline_rates = pd.read_excel(
        r'C:\\Users\LiZh\Desktop\P&G plant data\packing_line_rates.xlsx')
productionplan_August = pd.read_excel(r'C:\Users\LiZh\Desktop\P&G plant data\Production plans\2019August09_14h14_5days_schedule.xlsx')
meta_August = pd.read_excel(
        r'C:\Users\LiZh\Desktop\P&G plant data\meta_August.xlsx')

''' defining parameters '''
formulation_rate = 3660
num_lines = 12
num_products = 40   #
num_pack_line = 3
num_form_line = 3
population_size = 5
population = []
var = 2
num_children = population_size * var
K = 1
###############################################################################
'''  create necessary matrices  '''
###############################################################################
'''define the data frame'''
df = pd.DataFrame(meta_August, columns=['Brand code', 'Pouch printing',
                                        'Bottom liquid', 'Top liquid 1',
                                        'Top liquid 2', 'Tub size',
                                        'Tub color', 'Units per case', 'Count',
                                        'Case type', 'Pouch code'])

#random.seed(1)

def matrix(i):
    matrix = []
    co_times = [6, 12, 18, 25, 25, 30, 20, 150, 10, 60]
    for j in df.iloc[:, i-1]:
        matrix_1 = []
        for k in df.iloc[:, i-1]:
            if j == k:
                co_max = 0
                matrix_1.append(co_max)
            else:
                co_max = co_times[i-1]
                matrix_1.append(co_max)
        matrix.append(matrix_1)
    return matrix


def changeover_matrix(stage_num):
    i = 0
    matrix_co = np.zeros(np.shape(matrix(1)))
    stage = [[2, 3, 4, 5], [1, 6, 7, 8, 9, 10, 11]]
    # formulation stage and paking stage
    co_times = [6, 12, 18, 25, 25, 30, 20, 150, 10, 60]
    for co in co_times:
        i = i+1
        if i in stage[stage_num]:
            matrix_co = np.maximum(matrix_co, matrix(i))
    return matrix_co


def create_eligibility_matrix():
    eligibility_matrix = np.zeros((num_lines, num_products))
    eligibility_matrix[0:6, :] = np.ones((6, num_products))

    properties = [5, 7, 9]
    for i in range(0, num_products):
        help_frame = df.iloc[i, properties]
        # print(help_frame)
        for unit in range(0, np.shape(packingline_dedication)[0]):
            eligible = 1
            for prop in range(0, len(properties)):
                if not str(help_frame[prop]) in str(
                        packingline_dedication.iloc[unit, prop+1]):
                    # print("y")
                    eligible = 0
            eligibility_matrix[unit+6, i] = eligible
    return eligibility_matrix
eligibility_matrix = create_eligibility_matrix()

def create_packingline_rates_matrix():
    packingline_rates_matrix = np.zeros((num_lines, num_products))
    properties1 = [5, 7, 8]
    properties2 = [0, 1, 2]
    for i in range(0, num_products):
        help_frame1 = df.iloc[i, properties1]
        for unit in range(0, np.shape(packingline_dedication)[0]):
            if str(eligibility_matrix[unit+6, i]) == str(1.0):
                for j in range(0, np.shape(packingline_rates)[0]):
                    help_frame2 = packingline_rates.iloc[j, properties2]
                    if str(help_frame1[0]) in str(help_frame2[0]):
                        if str(help_frame1[1]) in str(help_frame2[1]):
                            if str(help_frame1[2]) in str(help_frame2[2]):
                                packingline_rates_matrix[unit+6, i] = packingline_rates.iloc[j, 3]
            else:
                packingline_rates_matrix[unit+6, i] = 0
    return packingline_rates_matrix
packingline_rates_matrix = create_packingline_rates_matrix()


'''
generate indi
'''
orderlist = range(0, num_products)

random_solution = [[33,
  31,
  36,
  20,
  4,
  8,
  1,
  28,
  17,
  2,
  11,
  18,
  9,
  3,
  12,
  10,
  7,
  38,
  32,
  16,
  25,
  6,
  30,
  15,
  0,
  13,
  19,
  26,
  14,
  39,
  24,
  22,
  35,
  37,
  29,
  34,
  27,
  23,
  21,
  5],
 [0,
  0,
  0,
  1,
  1,
  1,
  2,
  2,
  1,
  0,
  0,
  0,
  1,
  1,
  1,
  2,
  2,
  1,
  2,
  2,
  2,
  2,
  2,
  2,
  1,
  1,
  1,
  1,
  1,
  1,
  0,
  0,
  0,
  0,
  0,
  0,
  1,
  1,
  2,
  2]]

def create_form_indi(orderlist):
    individual = random.sample(orderlist, num_products)
    return individual

def find_eligi_line(product):
    help_list = []
    for line in range(num_pack_line):
        if eligibility_matrix[line + 6][product] == 1:
            help_list.append(line)
    return help_list


def create_packe_indi():
    individual = []
    for product in range(num_products):
        help_list = find_eligi_line(product)
        selected_line = random.choice(help_list)
        individual.append(selected_line)
    return individual


def create_complete_indi():
    complete_indi = []
    form_indi = create_form_indi(orderlist)
    pack_indi = create_packe_indi()
    complete_indi.append(form_indi)
    complete_indi.append(pack_indi)
    return complete_indi

'''time block'''
'''define time units'''
one_second = timedelta(seconds=1)
one_minute = timedelta(minutes=1)
one_hour = timedelta(hours=1)
one_day = timedelta(days=1)
start_date = datetime.strptime('1 Jan 2019 12:00AM', '%d %b %Y %I:%M%p')

'''
scheduler
'''
'''distribute products to form line, define corresponding timings'''
def func_form_stage(individual):
    earliest_starting_time_formline_matrix = [start_date, start_date, start_date]
    formline_allocation = []
    start_time_matrix_formline = []
    end_time_matrix_formline = []
    processing_time_matrix_formline = []
    co_form = []
    for i in range(0, num_form_line):
        formline_allocation.append([])
        start_time_matrix_formline.append([])
        end_time_matrix_formline.append([])
        processing_time_matrix_formline.append([])
        co_form.append([])
    for product in range(0, len(individual)):
        if product <= len(individual) - 2:
            earliest_formline_index = earliest_starting_time_formline_matrix.index(min(earliest_starting_time_formline_matrix))
            formline_allocation[earliest_formline_index].append(individual[product])
            start_time = min(earliest_starting_time_formline_matrix)
            start_time_matrix_formline[earliest_formline_index].append(start_time)
            processing_time = (meta_August.iloc[individual[product], 12] / formulation_rate)*one_minute
            processing_time_matrix_formline[earliest_formline_index].append(processing_time)
            end_time = start_time + processing_time
            end_time_matrix_formline[earliest_formline_index].append(end_time)
            '''update EST_matrix'''
            for stage in range(0,num_form_line):
                if len(formline_allocation[stage]) != 0:
                    changeover = (changeover_matrix(0)[formline_allocation[stage][-1]][individual[product + 1]])*one_minute
                    earliest_starting_time_formline_matrix[stage] = end_time_matrix_formline[stage][-1] + changeover
        else:
            earliest_formline_index = earliest_starting_time_formline_matrix.index(min(earliest_starting_time_formline_matrix))
            formline_allocation[earliest_formline_index].append(individual[product])
            start_time = min(earliest_starting_time_formline_matrix)
            start_time_matrix_formline[earliest_formline_index].append(start_time)
            processing_time = (meta_August.iloc[individual[product], 12] / formulation_rate)*one_minute
            processing_time_matrix_formline[earliest_formline_index].append(processing_time)
            end_time = start_time + processing_time
            end_time_matrix_formline[earliest_formline_index].append(end_time)
    return formline_allocation, start_time_matrix_formline, end_time_matrix_formline, processing_time_matrix_formline

result_formline = func_form_stage(random_solution[0])
formline_allocation = result_formline[0]
start_time_matrix_formline = result_formline[1]
end_time_matrix_formline = result_formline[2]
processing_time_matrix_formline = result_formline[3]

'''distribute products to pack line, define corresponding timings'''


def func_pack_stage(individual, formline_allocation, start_time_matrix_formline, end_time_matrix_formline):
    form_seq = individual[0]
    #pack_seq = individual[1][0]
    line_index_chrom = individual[1]
    #print('line_index_chrom: ',line_index_chrom)
    pack_allocation = [[], [], []]  #
    earliest_starting_time_packline_matrix = [start_date, start_date, start_date]
    start_time_matrix_packline = []
    end_time_matrix_packline = []
    processing_time_matrix_packline = []
    '''find pack allocation according to the seq it appears in form seq'''
    for search in form_seq:
        for i in range(num_products):
            if i == search:
                pack_allocation[line_index_chrom[i]].append(search)
    print('pack allo: ', pack_allocation)
    for i in range(0, num_pack_line):
        start_time_matrix_packline.append([])
        end_time_matrix_packline.append([])
        processing_time_matrix_packline.append([])

    def find_location(product, formline_allocation):
        for form_line in range(0, len(formline_allocation)):
            i = 0
            for search in formline_allocation[form_line]:
                if str(search) == str(product):
                    index_line = form_line
                    index_location = i
                else:
                    i = i + 1
        return index_line, index_location
    # assign timing
    '''find starting time of j on form line'''
    for stage in range(0, len(pack_allocation)):
        n = len(pack_allocation[stage])
        for num in range(0, n):
            if num <= n - 2:  # product 1 to n-1
                product = pack_allocation[stage][num]
                next_product = pack_allocation[stage][num + 1]
                location = find_location(product, formline_allocation)
                st_fj = start_time_matrix_formline[location[0]][location[1]]
                '''find est of previous job on pack line'''
                est = earliest_starting_time_packline_matrix[stage]
                '''find packing rate'''
                pr = packingline_rates_matrix[6 + stage][product]
                if st_fj >= est:
                    if pr <= formulation_rate:
                        '''append start time'''
                        st = st_fj
                        start_time_matrix_packline[stage].append(st)
                        '''calculate processing time'''
                        #print(meta_August.iloc[product, 12]/pr)
                        pt = (meta_August.iloc[product, 12]/pr)*one_minute
                        processing_time_matrix_packline[stage].append(pt)
                        '''calculate end time'''
                        et = st + pt
                        end_time_matrix_packline[stage].append(et)
                        '''update EST'''
                        co = (changeover_matrix(1)[product][next_product])*one_minute
                        earliest_starting_time_packline_matrix[stage] = et + co
                    else:
                        et = end_time_matrix_formline[location[0]][location[1]]
                        end_time_matrix_packline[stage].append(et)
                        pt = (meta_August.iloc[product, 12]/pr)*one_minute
                        processing_time_matrix_packline[stage].append(pt)
                        st = et - pt
                        start_time_matrix_packline[stage].append(st)
                        co = (changeover_matrix(1)[product][next_product])*one_minute
                        earliest_starting_time_packline_matrix[stage] = et + co
                else:
                    st = est
                    pt = (meta_August.iloc[product, 12]/pr)*one_minute
                    processing_time_matrix_packline[stage].append(pt)
                    et = st + pt
                    co = (changeover_matrix(1)[product][next_product])*one_minute
                    if et < end_time_matrix_formline[location[0]][location[1]]:
                        et = end_time_matrix_formline[location[0]][location[1]]
                        st = et - pt
                        end_time_matrix_packline[stage].append(et)
                        start_time_matrix_packline[stage].append(st)
                        earliest_starting_time_packline_matrix[stage] = et + co
                    else:
                        end_time_matrix_packline[stage].append(et)
                        start_time_matrix_packline[stage].append(st)
                        earliest_starting_time_packline_matrix[stage] = et + co
            else:  # product n
                product = pack_allocation[stage][num]
                location = find_location(product, formline_allocation)
                st_fj = start_time_matrix_formline[location[0]][location[1]]
                '''find est of previous job on pack line'''
                est = earliest_starting_time_packline_matrix[stage]
                '''find packing rate'''
                pr = packingline_rates_matrix[6 + stage][product]
                if st_fj >= est:
                    if pr <= formulation_rate:
                        st = st_fj
                        start_time_matrix_packline[stage].append(st)
                        '''calculate processing time'''
                        pt = (meta_August.iloc[product, 12]/pr)*one_minute
                        processing_time_matrix_packline[stage].append(pt)
                        '''calculate end time'''
                        et = st + pt
                        end_time_matrix_packline[stage].append(et)
                    else:
                        et = end_time_matrix_formline[location[0]][location[1]]
                        end_time_matrix_packline[stage].append(et)
                        pt = (meta_August.iloc[product, 12]/pr)*one_minute
                        processing_time_matrix_packline[stage].append(pt)
                        st = et - pt
                        start_time_matrix_packline[stage].append(st)
                else:
                    st = est
                    pt = (meta_August.iloc[product, 12]/pr)*one_minute
                    processing_time_matrix_packline[stage].append(pt)
                    et = st + pt
                    if et < end_time_matrix_formline[location[0]][location[1]]:
                        et = end_time_matrix_formline[location[0]][location[1]]
                        st = et - pt
                        end_time_matrix_packline[stage].append(et)
                        start_time_matrix_packline[stage].append(st)
                    else:
                        end_time_matrix_packline[stage].append(et)
                        start_time_matrix_packline[stage].append(st)
    return pack_allocation, start_time_matrix_packline, end_time_matrix_packline, processing_time_matrix_packline


result_packline = func_pack_stage(random_solution, formline_allocation, start_time_matrix_formline, end_time_matrix_formline)
packline_allocation = result_packline[0]
start_time_matrix_packline = result_packline[1]
end_time_matrix_packline = result_packline[2]
processing_time_matrix_packline = result_packline[3]


################################################################################
#''' plot the gantt chart '''
################################################################################
# Declaring a figure "gnt"
fig, gnt = plt.subplots()

# Setting Y-axis limits
gnt.set_ylim(0, 80)

# Setting X-axis limits
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
gnt.set_xlim(start_date, start_date + 3 * one_day)

# Setting labels for x-axis and y-axis
gnt.set_xlabel('Time')
gnt.set_ylabel('Units')
gnt.set_title('Gantt chart of best solution')
# Setting ticks on y-axis
gnt.set_yticks([13, 23, 33, 43, 53, 63])
# Labelling tickes of y-axis
gnt.set_yticklabels(['P3', 'P2', 'P1', 'F3', 'F2', 'F1'])

# Setting graph attribute
# gnt.grid(True)

# Declaring a bar in schedule
for form_stage in range(0, len(formline_allocation)):
    for product in range(0, len(formline_allocation[form_stage])):
        rect = gnt.broken_barh([((start_time_matrix_formline)[form_stage][product],(processing_time_matrix_formline)[form_stage][product])],(60-form_stage*10,6),facecolors =('tab:orange'))
        plt.text((start_time_matrix_formline)[form_stage][product]+((processing_time_matrix_formline)[form_stage][product])/2,63-form_stage*10,formline_allocation[form_stage][product],ha = 'center',va = 'center',fontsize=6)

for pack_stage in range(0, len(packline_allocation)):
    for product in range(0, len(packline_allocation[pack_stage])):
        rect = gnt.broken_barh([((start_time_matrix_packline)[pack_stage][product],(processing_time_matrix_packline)[pack_stage][product])],(30-pack_stage*10,6),facecolors =('tab:red'))
        plt.text((start_time_matrix_packline)[pack_stage][product]+((processing_time_matrix_packline)[pack_stage][product])/2,33-pack_stage*10,packline_allocation[pack_stage][product],ha = 'center',va = 'center',fontsize=6)
# add vertical dashed lines
x1 = start_date+1*one_day
x2 = start_date+2*one_day  # line at this x position
y1 = 0   # y limits of a line
y2 = 80
plt.plot((x1, x1), (y1, y2), ':', color='gray')
plt.plot((x2, x2), (y1, y2), ':', color='gray')
plt.savefig("gantt chart.png", dpi=800)


###############################################################################
'''Inventory profile in buffer'''
###############################################################################
fig, buffer = plt.subplots(1)
Bufferlimit = 3660 * 60 * 8 * 6  # pouches
buffer.set_xlim(start_date, start_date + 3*one_day)
buffer.set_ylim(0, 2e7)
buffer.set_xlabel('Time [Days]')
buffer.set_ylabel('Amount in buffer [Pouches]')
buffer.set_title('Buffer profile')


''' plot overall products inventory profile '''   
realtime=start_date
y0=0
x_matrix=[]
y_matrix=[]
product_produced=0
product_packed=0
while realtime <= start_date+ 3*one_day:
    num_formulating_product = 0
    for form_stage in range(0,len(formline_allocation)):
        form_product_index=0
        for form_product in formline_allocation[form_stage]:
            if realtime >= start_time_matrix_formline[form_stage][form_product_index] and realtime <= end_time_matrix_formline[form_stage][form_product_index]:
                num_formulating_product +=1
                form_product_index +=1
            else:
                form_product_index +=1
    for num in range(0,num_formulating_product):
        product_produced += formulation_rate
    num_packing_product=0
    for pack_stage in range(0,len(packline_allocation)):
        pack_product_index=0
        for pack_product in packline_allocation[pack_stage]:
            if realtime >= start_time_matrix_packline[pack_stage][pack_product_index] and realtime <= end_time_matrix_packline[pack_stage][pack_product_index]:
                num_packing_product +=1
                product_packed += packingline_rates_matrix[pack_stage+6][pack_product]
                pack_product_index +=1
            else:
                pack_product_index +=1
    x = realtime
    x_matrix.append(x)
    y = y0 + product_produced - product_packed
    y_matrix.append(y)
    realtime +=one_minute
plt.plot(x_matrix,y_matrix,'-')


''' plot product family inventory profile ''' 
pouchcode_family_matrix = []
for code in df.iloc[:,-1]:
    if code not in pouchcode_family_matrix:
        pouchcode_family_matrix.append(code)

counter = 0 
for code in pouchcode_family_matrix:
    search = code
    realtime=start_date
    y0=0
    x_matrix=[]
    y_matrix=[]
    product_produced=0
    product_packed=0
    while realtime <= start_date+ 3*one_day:
        for form_stage in range(0,len(formline_allocation)):
            form_product_index=0
            for form_product in formline_allocation[form_stage]:
                if realtime >= start_time_matrix_formline[form_stage][form_product_index] and realtime <= end_time_matrix_formline[form_stage][form_product_index] and str(df.iloc[form_product,-1]) == str(search):
                    product_produced += formulation_rate
                    form_product_index +=1
                else:
                    form_product_index +=1
        
        for pack_stage in range(0,len(packline_allocation)):
            pack_product_index=0
            for pack_product in packline_allocation[pack_stage]:
                if realtime >= start_time_matrix_packline[pack_stage][pack_product_index] and realtime <= end_time_matrix_packline[pack_stage][pack_product_index] and str(df.iloc[pack_product,-1]) == str(search):
                   product_packed += packingline_rates_matrix[pack_stage+6][pack_product]
                   pack_product_index +=1
                else:
                   pack_product_index +=1
        x = realtime
        x_matrix.append(x)
        y = y0 + product_produced - product_packed
        y_matrix.append(y)
        realtime +=one_minute
    plt.plot(x_matrix,y_matrix,'-',label= counter, linewidth=0.5)
    plt.legend(loc=1,title="pouch family", fontsize = 'xx-small',frameon=False)    
    counter +=1

        
#add upper buffer limit dashed lines
x1 = start_date
x2 = start_date+3*one_day 
y1 = Bufferlimit   
y2 = Bufferlimit
plt.plot((x1,x2),(y1,y2),':')      
        
plt.savefig("Buffer profile.png",dpi=800) 