import pandas as pd
import numpy as np
import random
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import timedelta

'''
notes: two chromos, linear ranking par selection, age based sur selection
'''
###############################################################################
'''  read file and define parameters   '''
###############################################################################
'''read excel file'''
co_times = pd.read_excel(
        r'C:\Users\LiZh\Desktop\P&G plant data\changeover_times.xlsx')
metadata = pd.read_excel(
        r'C:\\Users\LiZh\Desktop\P&G plant data\metadata_2.xlsx')
packingline_dedication = pd.read_excel(
        r'C:\Users\LiZh\Desktop\P&G plant data\packing_line_dedication.xlsx')
packingline_rates = pd.read_excel(
        r'C:\\Users\LiZh\Desktop\P&G plant data\packing_line_rates.xlsx')
productionplan_August = pd.read_excel(r'C:\Users\LiZh\Desktop\P&G plant data\Production plans\2019August09_14h14_5days_schedule.xlsx')
meta_August = pd.read_excel(
        r'C:\Users\LiZh\Desktop\P&G plant data\meta_August.xlsx')

''' defining parameters '''
formulation_rate = 3660
num_lines = 12
num_products = 40   #
num_pack_line = 3
num_form_line = 3
population_size = 5
population = []
var = 2
num_children = population_size * var
K = 1
###############################################################################
'''  create necessary matrices  '''
###############################################################################
'''define the data frame'''
df = pd.DataFrame(meta_August, columns=['Brand code', 'Pouch printing',
                                        'Bottom liquid', 'Top liquid 1',
                                        'Top liquid 2', 'Tub size',
                                        'Tub color', 'Units per case', 'Count',
                                        'Case type', 'Pouch code'])

random.seed(1)

def matrix(i):
    matrix = []
    co_times = [6, 12, 18, 25, 25, 30, 20, 150, 10, 60]
    for j in df.iloc[:, i-1]:
        matrix_1 = []
        for k in df.iloc[:, i-1]:
            if j == k:
                co_max = 0
                matrix_1.append(co_max)
            else:
                co_max = co_times[i-1]
                matrix_1.append(co_max)
        matrix.append(matrix_1)
    return matrix


def changeover_matrix(stage_num):
    i = 0
    matrix_co = np.zeros(np.shape(matrix(1)))
    stage = [[2, 3, 4, 5], [1, 6, 7, 8, 9, 10, 11]]
    # formulation stage and paking stage
    co_times = [6, 12, 18, 25, 25, 30, 20, 150, 10, 60]
    for co in co_times:
        i = i+1
        if i in stage[stage_num]:
            matrix_co = np.maximum(matrix_co, matrix(i))
    return matrix_co


def create_eligibility_matrix():
    eligibility_matrix = np.zeros((num_lines, num_products))
    eligibility_matrix[0:6, :] = np.ones((6, num_products))

    properties = [5, 7, 9]
    for i in range(0, num_products):
        help_frame = df.iloc[i, properties]
        # print(help_frame)
        for unit in range(0, np.shape(packingline_dedication)[0]):
            eligible = 1
            for prop in range(0, len(properties)):
                if not str(help_frame[prop]) in str(
                        packingline_dedication.iloc[unit, prop+1]):
                    # print("y")
                    eligible = 0
            eligibility_matrix[unit+6, i] = eligible
    return eligibility_matrix
eligibility_matrix = create_eligibility_matrix()

def create_packingline_rates_matrix():
    packingline_rates_matrix = np.zeros((num_lines, num_products))
    properties1 = [5, 7, 8]
    properties2 = [0, 1, 2]
    for i in range(0, num_products):
        help_frame1 = df.iloc[i, properties1]
        for unit in range(0, np.shape(packingline_dedication)[0]):
            if str(eligibility_matrix[unit+6, i]) == str(1.0):
                for j in range(0, np.shape(packingline_rates)[0]):
                    help_frame2 = packingline_rates.iloc[j, properties2]
                    if str(help_frame1[0]) in str(help_frame2[0]):
                        if str(help_frame1[1]) in str(help_frame2[1]):
                            if str(help_frame1[2]) in str(help_frame2[2]):
                                packingline_rates_matrix[unit+6, i] = packingline_rates.iloc[j, 3]
            else:
                packingline_rates_matrix[unit+6, i] = 0
    return packingline_rates_matrix
packingline_rates_matrix = create_packingline_rates_matrix()


'''
generate indi
'''
orderlist = range(0, num_products)


def create_form_indi(orderlist):
    individual = random.sample(orderlist, num_products)
    return individual

def find_eligi_line(product):
    help_list = []
    for line in range(num_pack_line):
        if eligibility_matrix[line + 6][product] == 1:
            help_list.append(line)
    return help_list


def create_packe_indi():
    individual = []
    for product in range(num_products):
        help_list = find_eligi_line(product)
        selected_line = random.choice(help_list)
        individual.append(selected_line)
    return individual


def create_complete_indi():
    complete_indi = []
    form_indi = create_form_indi(orderlist)
    pack_indi = create_packe_indi()
    complete_indi.append(form_indi)
    complete_indi.append(pack_indi)
    return complete_indi

'''time block'''
'''define time units'''
one_second = timedelta(seconds=1)
one_minute = timedelta(minutes=1)
one_hour = timedelta(hours=1)
one_day = timedelta(days=1)
start_date = datetime.strptime('1 Jan 2019 12:00AM', '%d %b %Y %I:%M%p')

'''
scheduler
'''
'''distribute products to form line, define corresponding timings'''
def func_form_stage(individual):
    earliest_starting_time_formline_matrix = [start_date, start_date, start_date]
    formline_allocation = []
    start_time_matrix_formline = []
    end_time_matrix_formline = []
    processing_time_matrix_formline = []
    for i in range(0, num_form_line):
        formline_allocation.append([])
        start_time_matrix_formline.append([])
        end_time_matrix_formline.append([])
        processing_time_matrix_formline.append([])
    for product in range(0, len(individual)):
        if product <= len(individual) - 2:
            earliest_formline_index = earliest_starting_time_formline_matrix.index(min(earliest_starting_time_formline_matrix))
            formline_allocation[earliest_formline_index].append(individual[product])
            start_time = min(earliest_starting_time_formline_matrix)
            start_time_matrix_formline[earliest_formline_index].append(start_time)
            processing_time = (meta_August.iloc[individual[product], 12] / formulation_rate)*one_minute
            processing_time_matrix_formline[earliest_formline_index].append(processing_time)
            end_time = start_time + processing_time
            end_time_matrix_formline[earliest_formline_index].append(end_time)
            '''update EST_matrix'''
            for stage in range(0,num_form_line):
                if len(formline_allocation[stage]) != 0:
                    changeover = (changeover_matrix(0)[formline_allocation[stage][-1]][individual[product + 1]])*one_minute
                    earliest_starting_time_formline_matrix[stage] = end_time_matrix_formline[stage][-1] + changeover
        else:
            earliest_formline_index = earliest_starting_time_formline_matrix.index(min(earliest_starting_time_formline_matrix))
            formline_allocation[earliest_formline_index].append(individual[product])
            start_time = min(earliest_starting_time_formline_matrix)
            start_time_matrix_formline[earliest_formline_index].append(start_time)
            processing_time = (meta_August.iloc[individual[product], 12] / formulation_rate)*one_minute
            processing_time_matrix_formline[earliest_formline_index].append(processing_time)
            end_time = start_time + processing_time
            end_time_matrix_formline[earliest_formline_index].append(end_time)
    return formline_allocation, start_time_matrix_formline, end_time_matrix_formline, processing_time_matrix_formline


'''distribute products to pack line, define corresponding timings'''


def func_pack_stage(individual, formline_allocation, start_time_matrix_formline, end_time_matrix_formline):
    form_seq = individual[0]
    #pack_seq = individual[1][0]
    line_index_chrom = individual[1]
    #print('line_index_chrom: ',line_index_chrom)
    pack_allocation = [[], [], []]  #
    earliest_starting_time_packline_matrix = [start_date, start_date, start_date]
    start_time_matrix_packline = []
    end_time_matrix_packline = []
    processing_time_matrix_packline = []
    '''find pack allocation according to the seq it appears in form seq'''
    for search in form_seq:
        for i in range(num_products):
            if i == search:
                pack_allocation[line_index_chrom[i]].append(search)
    #print('pack allo: ', pack_allocation)
    for i in range(0, num_pack_line):
        start_time_matrix_packline.append([])
        end_time_matrix_packline.append([])
        processing_time_matrix_packline.append([])

    def find_location(product, formline_allocation):
        for form_line in range(0, len(formline_allocation)):
            i = 0
            for search in formline_allocation[form_line]:
                if str(search) == str(product):
                    index_line = form_line
                    index_location = i
                else:
                    i = i + 1
        return index_line, index_location
    # assign timing
    '''find starting time of j on form line'''
    for stage in range(0, len(pack_allocation)):
        n = len(pack_allocation[stage])
        for num in range(0, n):
            if num <= n - 2:  # product 1 to n-1
                product = pack_allocation[stage][num]
                next_product = pack_allocation[stage][num + 1]
                location = find_location(product, formline_allocation)
                st_fj = start_time_matrix_formline[location[0]][location[1]]
                '''find est of previous job on pack line'''
                est = earliest_starting_time_packline_matrix[stage]
                '''find packing rate'''
                pr = packingline_rates_matrix[6 + stage][product]
                if st_fj >= est:
                    if pr <= formulation_rate:
                        '''append start time'''
                        st = st_fj
                        start_time_matrix_packline[stage].append(st)
                        '''calculate processing time'''
                        #print(meta_August.iloc[product, 12]/pr)
                        pt = (meta_August.iloc[product, 12]/pr)*one_minute
                        processing_time_matrix_packline[stage].append(pt)
                        '''calculate end time'''
                        et = st + pt
                        end_time_matrix_packline[stage].append(et)
                        '''update EST'''
                        co = (changeover_matrix(1)[product][next_product])*one_minute
                        earliest_starting_time_packline_matrix[stage] = et + co
                    else:
                        et = end_time_matrix_formline[location[0]][location[1]]
                        end_time_matrix_packline[stage].append(et)
                        pt = (meta_August.iloc[product, 12]/pr)*one_minute
                        processing_time_matrix_packline[stage].append(pt)
                        st = et - pt
                        start_time_matrix_packline[stage].append(st)
                        co = (changeover_matrix(1)[product][next_product])*one_minute
                        earliest_starting_time_packline_matrix[stage] = et + co
                else:
                    st = est
                    pt = (meta_August.iloc[product, 12]/pr)*one_minute
                    processing_time_matrix_packline[stage].append(pt)
                    et = st + pt
                    co = (changeover_matrix(1)[product][next_product])*one_minute
                    if et < end_time_matrix_formline[location[0]][location[1]]:
                        et = end_time_matrix_formline[location[0]][location[1]]
                        st = et - pt
                        end_time_matrix_packline[stage].append(et)
                        start_time_matrix_packline[stage].append(st)
                        earliest_starting_time_packline_matrix[stage] = et + co
                    else:
                        end_time_matrix_packline[stage].append(et)
                        start_time_matrix_packline[stage].append(st)
                        earliest_starting_time_packline_matrix[stage] = et + co
            else:  # product n
                product = pack_allocation[stage][num]
                location = find_location(product, formline_allocation)
                st_fj = start_time_matrix_formline[location[0]][location[1]]
                '''find est of previous job on pack line'''
                est = earliest_starting_time_packline_matrix[stage]
                '''find packing rate'''
                pr = packingline_rates_matrix[6 + stage][product]
                if st_fj >= est:
                    if pr <= formulation_rate:
                        st = st_fj
                        start_time_matrix_packline[stage].append(st)
                        '''calculate processing time'''
                        pt = (meta_August.iloc[product, 12]/pr)*one_minute
                        processing_time_matrix_packline[stage].append(pt)
                        '''calculate end time'''
                        et = st + pt
                        end_time_matrix_packline[stage].append(et)
                    else:
                        et = end_time_matrix_formline[location[0]][location[1]]
                        end_time_matrix_packline[stage].append(et)
                        pt = (meta_August.iloc[product, 12]/pr)*one_minute
                        processing_time_matrix_packline[stage].append(pt)
                        st = et - pt
                        start_time_matrix_packline[stage].append(st)
                else:
                    st = est
                    pt = (meta_August.iloc[product, 12]/pr)*one_minute
                    processing_time_matrix_packline[stage].append(pt)
                    et = st + pt
                    if et < end_time_matrix_formline[location[0]][location[1]]:
                        et = end_time_matrix_formline[location[0]][location[1]]
                        st = et - pt
                        end_time_matrix_packline[stage].append(et)
                        start_time_matrix_packline[stage].append(st)
                    else:
                        end_time_matrix_packline[stage].append(et)
                        start_time_matrix_packline[stage].append(st)
    return pack_allocation, start_time_matrix_packline, end_time_matrix_packline, processing_time_matrix_packline


###############################################################################
'''Genetic alorithm'''
###############################################################################
# population initialization
for individual in range(0, population_size):
    population.append(create_complete_indi())

'''find fitness value'''
T_target = 1


def find_fitness(individual, packingline_rates_matrix):
    form_line_max_completiontime_matrix = [start_date]
    result_formline = func_form_stage(individual[0])
    formline_allocation = result_formline[0]
    start_time_matrix_formline = result_formline[1]
    end_time_matrix_formline = result_formline[2]
    result_packline = func_pack_stage(individual, formline_allocation, start_time_matrix_formline, end_time_matrix_formline)
    packline_allocation = result_packline[0]
    end_time_matrix_packline = result_packline[2]

    for form_stage in range(0, len(formline_allocation)):
        if (end_time_matrix_formline)[form_stage][-1] > max(
                form_line_max_completiontime_matrix):
            form_line_max_completiontime_matrix.append(
                    (end_time_matrix_formline)[form_stage][-1])
    pack_line_max_completiontime_matrix = [start_date]

    for pack_stage in range(0, len(packline_allocation)):
        if len(packline_allocation[pack_stage]) != 0 and (
                end_time_matrix_packline)[pack_stage][-1] > max(
                        pack_line_max_completiontime_matrix):
            pack_line_max_completiontime_matrix.append(
                    (end_time_matrix_packline)[pack_stage][-1])
    if max(form_line_max_completiontime_matrix) >= max(
            pack_line_max_completiontime_matrix):
        max_completion_time = max(form_line_max_completiontime_matrix)
        makespan = max_completion_time - start_date
        fitness = T_target/(makespan.days*24*60 + makespan.seconds/60)
        makespan = makespan.days*24 + makespan.seconds/3600
    else:
        max_completion_time = max(pack_line_max_completiontime_matrix)
        makespan = max_completion_time - start_date
        fitness = T_target/(makespan.days*24*60 + makespan.seconds/60)
        makespan = makespan.days*24 + makespan.seconds/3600
    return fitness, makespan


def create_fitness_matrix(population, packingline_rates_matrix):
    fitness_matrix = []
    makespan_matrix = []
    for pop in population:
        result = find_fitness(pop, packingline_rates_matrix)
        fitness = result[0]
        makespan = result[1]
        fitness_matrix.append(fitness)
        makespan_matrix.append(makespan)
    return fitness_matrix, makespan_matrix


def calculate_total_fitness(fitness_matrix):
    total_fitness = 0
    for indi in range(0, len(fitness_matrix)):
        total_fitness += fitness_matrix[indi]
    return total_fitness


def create_probability_matrix(fitness_matrix, packingline_rates_matrix):
    probability_matrix = []
    total_fitness = calculate_total_fitness(fitness_matrix)
    for indi in range(0, len(fitness_matrix)):
        probability_matrix.append(fitness_matrix[indi]/total_fitness)
    return probability_matrix


'''parent selection based on linear ranking '''
def parent_selection(population, probability_matrix):
    selected_parent = []
    probability_rank_based = []
    variable_s = 1.8
    '''linear ranking'''
    x = probability_matrix
    seq = sorted(x)
    rank = [seq.index(v) for v in x]
    n = len(population)
    for j in rank:
        prob = (2-variable_s)/n + (2*j*(variable_s-1))/(n*(n-1))
        probability_rank_based.append(prob)
    '''linear ranking'''
    for num in range(0, num_children):
        parent = []
        fixed_point1 = random.random()
        if fixed_point1 <= 0.5:
            Max = fixed_point1 + 0.5
            Min = fixed_point1
        else:
            Max = fixed_point1
            Min = fixed_point1 - 0.5
        isum = 0
        j = 0
        while isum < Min and j < population_size:
            isum += probability_rank_based[j]
            j += 1
        parent.append(population[j-1])
    #        isum = 0
    #        j = 0
        while isum < Max and j < population_size:
            isum += probability_rank_based[j]
            j += 1
        parent.append(population[j-1])
        selected_parent.append(parent)
    return selected_parent


def crossover(selected_parent):
    children_after_crossover = []
    for parent in selected_parent:
        '''find two cut points'''
        child = []
        cutpoint1_f = int(random.random() * num_products)
        cutpoint2_f = int(random.random() * num_products)
        startGene_f = min(cutpoint1_f, cutpoint2_f)
        endGene_f = max(cutpoint1_f, cutpoint2_f)

        cutpoint1_p = int(random.random() * num_products)
        cutpoint2_p = int(random.random() * num_products)
        startGene_p = min(cutpoint1_p, cutpoint2_p)
        endGene_p = max(cutpoint1_p, cutpoint2_p)

        helplist_f = []
        childP1_f = parent[0][0][startGene_f:endGene_f]
        childP2_f = parent[1][0][endGene_f:num_products]
        childP3_f = parent[1][0][0:endGene_f]
        par_revised_f = childP2_f + childP3_f
        for gene in par_revised_f:
            if gene not in childP1_f:
                helplist_f.append(gene)
        help1_f = helplist_f[0:num_products-endGene_f]
        help2_f = helplist_f[num_products-endGene_f:len(helplist_f)]
        child_f = help2_f + childP1_f + help1_f

        childP1_line = parent[0][1][startGene_p:endGene_p]
        childP2_line = parent[1][1][endGene_p:num_products]
        childP3_line = parent[1][1][0:startGene_p]
        child_line = childP3_line + childP1_line + childP2_line

        child.append(child_f)
        child.append(child_line)
        children_after_crossover.append(child)
    return children_after_crossover


'''mutation operator using displacement mutation'''
mutationRate = 0.5   # 


def mutation(pop_before_mutation, mutationRate):
    children_after_mutation = []
    for children in pop_before_mutation:
        if (random.random() < mutationRate):
            complete_child = []
            cutpoint1_f = int(random.random() * num_products)
            cutpoint2_f = int(random.random() * num_products)
            startGene_f = min(cutpoint1_f, cutpoint2_f)
            endGene_f = max(cutpoint1_f, cutpoint2_f)

            cutpoint1_p = int(random.random() * num_products)
            cutpoint2_p = int(random.random() * num_products)
            startGene_p = min(cutpoint1_p, cutpoint2_p)
            endGene_p = max(cutpoint1_p, cutpoint2_p)

            childP1_form = children[0][startGene_f:endGene_f]
            childP2_form = children[0][0:startGene_f]
            childP3_form = children[0][endGene_f:num_products]
            childP4_form = childP2_form + childP3_form
            cutpoint_f = int(random.random() * len(childP4_form))

            for child in childP1_form:
                childP4_form.insert(cutpoint_f, child)
                cutpoint_f += 1

            for product in range(startGene_p, endGene_p):
                help_list = find_eligi_line(product)
                if len(help_list) > 1:
                    help_list.remove(children[1][product])
                    new_line = random.choice(help_list)
                    children[1][product] = new_line
            complete_child.append(childP4_form)
            complete_child.append(children[1])
            children_after_mutation.append(complete_child)
        else:
            children_after_mutation.append(children)
    return children_after_mutation


'''generate a new population by adding children after mutation'''


def create_newpop(population, children_after_mutation):
    new_population = population + children_after_mutation
    return new_population


def compute_child(children_after_mutation, packingline_rates_matrix):
    makespan_child = []
    fitness_child = []
    for i in children_after_mutation:
        #print('this is child : ', i)
        result_children = find_fitness(i, packingline_rates_matrix)
        fitness_child.append(result_children[0])
        makespan_child.append(result_children[1])
    return fitness_child, makespan_child


def create_newmakespanmatrix(makespan_matrix, makespan_child):
    new_makespan_matrix = makespan_matrix + makespan_child
    return new_makespan_matrix


'''
change to age based
offspring size has to be at least the same as pop
set pop = 5
    off = 10, 20 , 35
    see above parameters
'''


def create_new_age_matrix(age_pop, age_child):
    new_age_matrix = age_pop + age_child
    return new_age_matrix


def survivor_selection(makespan_child, age_pop, population, new_population, children_after_mutation, makespan_matrix, new_makespan_matrix, packingline_rates_matrix):
    age = 2
    new_age_matrix = create_new_age_matrix(age_pop, age_child)
    age_new_par = []
    '''based on elitism while <= age'''
    if max(new_age_matrix) <= age:
        next_generation = []
        next_makespan_matrix = []
        in_arr = np.array(new_makespan_matrix)
        order = np.argsort(in_arr)
        for i in range(0, population_size):   #
            next_generation.append(new_population[order[i]])   #
            next_makespan_matrix.append(new_makespan_matrix[order[i]])
            age_new_par.append(new_age_matrix[order[i]])
        age_new_par = [x + K for x in age_new_par] # increment by one
        age_pop = age_new_par
    else:
        '''delete oldest'''
        index_list = []
        for item_num in range(0, len(age_pop)):
            if age_pop[item_num] > age:
                index_list.append(item_num)

        index_list.reverse()
        #print('index list :',index_list)
        for item in index_list:
            dead_list[0].append(population[item])
            del population[item]
            dead_list[1].append(makespan_matrix[item])
            del makespan_matrix[item]
            del age_pop[item]
        '''adding new indi'''
        n = len(index_list)
        makespan_child = makespan_child
        in_arr = np.array(makespan_child)
        order = np.argsort(in_arr)
        for i in range(0, n):   #
            population.append(children_after_mutation[order[i]])  #
            age_pop.append(0)
            makespan_matrix.append(makespan_child[order[i]])
        age_pop = [x + K for x in age_pop]
        next_generation = population
        next_makespan_matrix = makespan_matrix
    return next_generation, next_makespan_matrix, age_pop


num_generation = 250
all_bestindi_makespan_matrix = []
age_child = []
dead_list = [[], []]  # chromo,makespan
dead_loc = []
dead_value = []
for i in range(0, num_children):
    age_child.append(0)


def run_simulation(population, packingline_rates_matrix):
    age_pop = []
    for i in range(0, population_size):
        age_pop.append(1)
    i = 1
    best_individual_matrix = []
    fig, evo = plt.subplots()         # plot evolution of makespan of each gen.
    x_matrix = [0]
    y_matrix = []
    z_matrix = []
    while i <= num_generation:
        print("This is generation " + str(i))
        result_parent = create_fitness_matrix(population, packingline_rates_matrix)
        fitness_matrix = result_parent[0]
        makespan_matrix = result_parent[1]
        if i == 1:
            mean_makespan = sum(makespan_matrix)/len(makespan_matrix)
            z_matrix.append(mean_makespan)
            best = min(makespan_matrix)
            y_matrix.append(best)
            worst_makespan = max(makespan_matrix)
            worst_loc = 0
        #print('makespan of gen: ' + str(i), makespan_matrix)
        probability_matrix = create_probability_matrix(
            fitness_matrix, packingline_rates_matrix)  # create prob matrix
        '''select par'''
        selected_parent = parent_selection(population, probability_matrix)
        '''recombination'''
        children_after_crossover = crossover(selected_parent)
        '''mutation'''
        children_after_mutation = mutation(children_after_crossover, mutationRate)
        '''combine par and child'''
        new_population = create_newpop(population, children_after_mutation)
        '''compute child'''
        result_child = compute_child(children_after_mutation, packingline_rates_matrix)
        makespan_child = result_child[1]
        #print('makespan child: ',makespan_child)
        new_makespan_matrix = create_newmakespanmatrix(makespan_matrix, makespan_child)
        #print('new mapkespan: ',new_makespan_matrix)
        '''compute next gen.'''
        result_next_gen = survivor_selection(makespan_child, age_pop,
        population, new_population, children_after_mutation, makespan_matrix, 
        new_makespan_matrix, packingline_rates_matrix)
 
        age_pop = result_next_gen[2]
        #print('age of pop: ', age_pop)
        population = result_next_gen[0]
        next_makespan_matrix = result_next_gen[1]
        #print('next gen makespan: ', next_makespan_matrix)
        mean_makespan = sum(next_makespan_matrix)/len(next_makespan_matrix)
        #print('mean maakespan: ', mean_makespan)
        '''find best indi'''
        in_arr = np.array(next_makespan_matrix)
        order = np.argsort(in_arr)

        x_matrix.append(i)
        z_matrix.append(mean_makespan)
        y = min(next_makespan_matrix)
        '''compare with dead list'''
        y_dead = 1000
        if len(dead_list[1]) != 0:
            y_dead = min(dead_list[1])  # minimum makespan
            index = dead_list[1].index(y_dead)  # indi index
            indi_dead = dead_list[0][index]
        else:
            pass
        if y <= y_dead:
            y_matrix.append(y)
            best_individual_matrix.append(population[order[0]])
            all_bestindi_makespan_matrix.append(y)
        else:
            y_matrix.append(y_dead)
            dead_loc.append(i)
            dead_value.append(y_dead)
            best_individual_matrix.append(indi_dead)
            all_bestindi_makespan_matrix.append(y_dead)
        i += 1
    in_arr = np.array(all_bestindi_makespan_matrix)
    order = np.argsort(in_arr)
    best_individual = best_individual_matrix[order[0]]
    best_makespan = all_bestindi_makespan_matrix[order[0]]
    best_loc = num_generation
    plt.plot(x_matrix, y_matrix, "b--", linewidth=1)
    plt.plot(x_matrix, z_matrix, "g--", linewidth=1)
    plt.scatter(x=dead_loc, y=dead_value, color='red', s=1**2)
    plt.scatter(x=best_loc, y=best_makespan, color='gray', s=1)
    plt.scatter(x=worst_loc, y=worst_makespan, color='gray', s=1)
    plt.text(best_loc + 0.6, best_makespan + 0.3, 'Best', horizontalalignment='right', size='small', color='black', weight='semibold')
    plt.text(worst_loc + 0.6, worst_makespan, 'Worst', horizontalalignment='left', size='small', color='black', weight='semibold')
    evo.legend(('best_value', 'mean_value', 'from death list'), loc=1, frameon=False)
    evo.set_xlabel('Generations')
    evo.set_ylabel('Makespan [Hours]')
    evo.set_title('Evolution of makespan')
    plt.savefig("Makespan evolution new.png", dpi=1200)
#    completiontime_matrix = []
#    for indi in best_individual_matrix:
#        completiontime_matrix.append(find_completiontime(indi, packingline_rates_matrix))
#    fig, comp = plt.subplots()
#    plt.plot(x_matrix, completiontime_matrix, '.', color="green")
#    comp.set_xlabel('Generations')
#    comp.set_ylabel('Total completiontime [Hours]')
#    comp.set_title('Evolution of Total completiontime')
#    plt.savefig("Total completiontime evolution.png", dpi=800)
    print('death list: ', dead_value)
    print('worst mkespan: ', worst_makespan)
    print('best makespan: ', all_bestindi_makespan_matrix[order[0]])
    return best_individual
best_individual = run_simulation(population, packingline_rates_matrix)
