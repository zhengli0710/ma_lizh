import pandas as pd
import numpy as np
import random
import matplotlib.pyplot as plt 
from datetime import datetime  
from datetime import timedelta 
#from plotly import figure_factory as ff
#import sys
#
#
#import matplotlib.font_manager as font_manager
#import matplotlib.dates as mdates
#import logging

###############################################################################
'''  read file and define parameters   '''
###############################################################################
'''read excel file'''
co_times = pd.read_excel(r'C:\Users\LiZh\Desktop\P&G plant data\changeover_times.xlsx')
metadata = pd.read_excel(r'C:\Users\LiZh\Desktop\P&G plant data\metadata_2.xlsx')
packingline_dedication = pd.read_excel(r'C:\Users\LiZh\Desktop\P&G plant data\packing_line_dedication.xlsx')
packingline_rates = pd.read_excel(r'C:\Users\LiZh\Desktop\P&G plant data\packing_line_rates.xlsx')
productionplan_August = pd.read_excel(r'C:\Users\LiZh\Desktop\P&G plant data\Production plans\2019August09_14h14_5days_schedule.xlsx')
meta_August = pd.read_excel(r'C:\Users\LiZh\Desktop\P&G plant data\meta_August.xlsx')


''' defining parameters '''
formulation_rate = 3660
num_lines = 12
num_products = 18
num_pack_line = 3
num_form_line = 3

###############################################################################
'''  create necessary matrices  '''
###############################################################################
'''define the data frame'''
df = pd.DataFrame(meta_August, columns= ['Brand code','Pouch printing','Bottom liquid','Top liquid 1','Top liquid 2','Tub size','Tub color','Units per case','Count','Case type'])


def matrix(i):
    matrix=[]
    co_times = [6,12,18,25,25,30,20,150,10,60]
    for j in df.iloc[:,i-1]:
        matrix_1 = []
        for k in df.iloc[:,i-1]:
           if j == k:
              co_max=0
              matrix_1.append(co_max)
           else:
              co_max=co_times[i-1]
              matrix_1.append(co_max)   
        matrix.append(matrix_1)
    return matrix


def changeover_matrix(stage_num):
    i = 0
    matrix_co =  np.zeros(np.shape(matrix(1)))
    stage = [[2,3,4,5], [1,6,7,8,9,10,11]]     #formulation stage and paking stage
    co_times = [6,12,18,25,25,30,20,150,10,60]
    for co in co_times:
        i = i+1
        if i in stage[stage_num]:
            matrix_co = np.maximum(matrix_co, matrix(i))
    return matrix_co


def create_eligibility_matrix():   
    eligibility_matrix = np.zeros((num_lines,num_products))
    eligibility_matrix[0:6,:] = np.ones((6,num_products))
    
    properties = [5,7,9]
    for i in range(0, num_products):
        help_frame = df.iloc[i, properties]
        #print(help_frame)
        for unit in range(0, np.shape(packingline_dedication)[0]):
            eligible = 1
            for prop in range(0, len(properties)):
                if not str(help_frame[prop]) in str(packingline_dedication.iloc[unit, prop+1]):
                    #print("y")
                    eligible = 0
            eligibility_matrix[unit+6, i] = eligible
    return eligibility_matrix
eligibility_matrix = create_eligibility_matrix()


def create_packingline_rates_matrix():
    packingline_rates_matrix = np.zeros((num_lines,num_products))
    properties1 = [5,7,8]
    properties2 = [0,1,2]
    for i in range(0,num_products):
        help_frame1 = df.iloc[i,properties1]
        for unit in range(0, np.shape(packingline_dedication)[0]):
            if str(eligibility_matrix[unit+6,i]) == str(1.0):
                for j in range(0,np.shape(packingline_rates)[0]):
                    help_frame2 = packingline_rates.iloc[j,properties2]
                    if str(help_frame1[0]) in str(help_frame2[0]):
                        if str(help_frame1[1]) in str(help_frame2[1]):
                            if str(help_frame1[2]) in str(help_frame2[2]):
                                packingline_rates_matrix[unit+6,i] = packingline_rates.iloc[j,3]
            else:
                packingline_rates_matrix[unit+6,i] = 0
    return packingline_rates_matrix
packingline_rates_matrix = create_packingline_rates_matrix()


###############################################################################
'''  generate random solutions  '''
###############################################################################
'''generating an individual'''
orderlist=range(0,num_products)
def creatindividual(orderlist):
    random.seed(1)
    individual = random.sample(orderlist,num_products)
    return individual
random_solution = creatindividual(orderlist)



###############################################################################
'''  allocate products to formulation and packing lines  '''
###############################################################################
def form_line_products_allocation():
    form_line_allocation_matrix = []
    for matrix in range(0,num_form_line):
       form_line_allocation_matrix.append([]) 
    for products in range(0,len(random_solution)):
            if (products%num_form_line == 0) is True:
                form_line_allocation_matrix[num_form_line-3].append(random_solution[products])
            elif (products%num_form_line == 1) is True:
                form_line_allocation_matrix[num_form_line-2].append(random_solution[products])
            elif (products%num_form_line == 2) is True:
                form_line_allocation_matrix[num_form_line-1].append(random_solution[products])
    return form_line_allocation_matrix            
form_line_products_allocation = form_line_products_allocation()    
#print(form_line_products_allocation)    


def pack_line_products_allocation():
    pack_line_allocation_matrix = []
    for matrix in range(0,num_pack_line):
        pack_line_allocation_matrix.append([])
    num_schedule_matrix = 0
    for i in range(0,len(pack_line_allocation_matrix)):
        num_schedule_matrix = max(num_schedule_matrix,len(form_line_products_allocation[i]))
    schedule_matrix = []
    for matrix in range(0,num_schedule_matrix):
        schedule_matrix.append([])
    for num in range(0,num_schedule_matrix):
        for line in range(0,num_pack_line):
            schedule_matrix[num].append(form_line_products_allocation[line][num])
    #print(schedule_matrix)
    
    for matrix in range(0,len(schedule_matrix)):
        pack_line_states = ['idle','idle','idle']
        for form_line in range(0,num_form_line):
            for pack_line in range(0,num_pack_line):###check eligibility of each pack line
                if eligibility_matrix[pack_line+6][schedule_matrix[matrix][form_line]]==1 and \
                      pack_line_states[pack_line] =='idle':
                      pack_line_allocation_matrix[pack_line].append(schedule_matrix[matrix][form_line])                      
                      pack_line_states[pack_line] = 'busy'
                      break        
    return pack_line_allocation_matrix
pack_line_products_allocation = pack_line_products_allocation()    
#print(pack_line_products_allocation)    


###############################################################################
'''  assign start and end time to each product on both formulation and packing lines '''
###############################################################################
'''define time units'''
one_second = timedelta(seconds=1)
one_minute = timedelta(minutes=1)
one_hour = timedelta(hours=1)
one_day = timedelta(days=1)
start_date = datetime.strptime('1 Jan 2019 12:00AM', '%d %b %Y %I:%M%p')


''' Calculate start time and end time of formulation stage'''
formline_start_date = []
formline_end_date = []
formline_processingtime_matrix = []
for num in range(0,num_form_line):
    formline_start_date.append([])
for num in range(0,num_form_line):
    formline_end_date.append([])
for num in range(0,num_form_line):
    formline_processingtime_matrix.append([])
for form_stage in range(0,len(form_line_products_allocation)):
    start_date = datetime.strptime('1 Jan 2019 12:00AM', '%d %b %Y %I:%M%p')
    #Iterator = iter(form_line_products_allocation[form_stage])
    i = 0
    for product in form_line_products_allocation[form_stage]:
        i = i+1
        if i < len(form_line_products_allocation[form_stage]):
            formline_start_date[form_stage].append(start_date)
            processing_time = (meta_August.iloc[product,12]/formulation_rate)*one_minute
            formline_processingtime_matrix[form_stage].append(processing_time)
            end_time = start_date + processing_time
            formline_end_date[form_stage].append(end_time)
            start_date = end_time + (changeover_matrix(0)[product][form_line_products_allocation[form_stage][i]])*one_minute
        else:
            formline_start_date[form_stage].append(start_date)
            processing_time = (meta_August.iloc[product,12]/formulation_rate)*one_minute
            formline_processingtime_matrix[form_stage].append(processing_time)
            end_time = start_date + processing_time
            formline_end_date[form_stage].append(end_time)
            break
#print('formulation line start time:')
#print(formline_start_date)
#print('')
#print('formulation line end time:')
#print(formline_end_date)            
#print(formline_processingtime_matrix)
            
''' Calculate start time and end time of packing stage'''
packline_start_date = []
packline_end_date = []
packline_processingtime_matrix = []
for num in range(0,num_pack_line):
    packline_start_date.append([])
for num in range(0,num_pack_line):
    packline_end_date.append([])
for num in range(0,num_pack_line):
    packline_processingtime_matrix.append([])
for pack_stage in range(0,len(pack_line_products_allocation)):
    start_date = datetime.strptime('1 Jan 2019 12:00AM', '%d %b %Y %I:%M%p')
    time_block = 4*one_hour
    start_time = start_date + time_block
    i = 0
    for product in pack_line_products_allocation[pack_stage]:
        i = i + 1
        if i < len(pack_line_products_allocation[pack_stage]):
            packline_start_date[pack_stage].append(start_time)
            processing_time = (meta_August.iloc[product,12]/packingline_rates_matrix[pack_stage+6][product])*one_minute
            packline_processingtime_matrix[pack_stage].append(processing_time)
            end_time = start_time + processing_time
            packline_end_date[pack_stage].append(end_time)
            start_time = end_time + (changeover_matrix(1)[product][pack_line_products_allocation[pack_stage][i]])*one_minute
        else:
            packline_start_date[pack_stage].append(start_time)
            processing_time = (meta_August.iloc[product,12]/packingline_rates_matrix[pack_stage+6][product])*one_minute
            packline_processingtime_matrix[pack_stage].append(processing_time)
            end_time = start_time + processing_time
            packline_end_date[pack_stage].append(end_time)
#print('')
#print('packline_start_date')
#print(packline_start_date)
#print('')
#print('packline_end_date')
#print(packline_end_date)        
        
###############################################################################   
''' plot the gantt chart '''
###############################################################################
# Declaring a figure "gnt" 
fig, gnt = plt.subplots() 
  
# Setting Y-axis limits 
gnt.set_ylim(0, 80) 
  
# Setting X-axis limits 
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
gnt.set_xlim(start_date,start_date+ 3*one_day ) 

# Setting labels for x-axis and y-axis 
gnt.set_xlabel('Time') 
gnt.set_ylabel('Units') 
gnt.set_title('Gantt chart example') 
# Setting ticks on y-axis 
gnt.set_yticks([13, 23, 33, 43, 53, 63]) 
# Labelling tickes of y-axis 
gnt.set_yticklabels(['P3','P2','P1','F3', 'F2', 'F1']) 
  
# Setting graph attribute 
#gnt.grid(True) 

# Declaring a bar in schedule 
for form_stage in range(0,len(form_line_products_allocation)):
    for product in range(0,len(form_line_products_allocation[form_stage])):
        rect = gnt.broken_barh([(formline_start_date[form_stage][product],formline_processingtime_matrix[form_stage][product])],(60-form_stage*10,6),facecolors =('tab:orange'))
        plt.text(formline_start_date[form_stage][product]+(formline_processingtime_matrix[form_stage][product])/2,63-form_stage*10,form_line_products_allocation[form_stage][product],ha = 'center',va = 'center',fontsize=8)

for pack_stage in range(0,len(pack_line_products_allocation)):
    for product in range(0,len(pack_line_products_allocation[pack_stage])):
        rect = gnt.broken_barh([(packline_start_date[pack_stage][product],packline_processingtime_matrix[pack_stage][product])],(30-pack_stage*10,6),facecolors =('tab:red'))
        plt.text(packline_start_date[pack_stage][product]+(packline_processingtime_matrix[pack_stage][product])/2,33-pack_stage*10,pack_line_products_allocation[pack_stage][product],ha = 'center',va = 'center',fontsize=8)
#add vertical dashed lines
x1 = start_date+1*one_day
x2 = start_date+2*one_day # line at this x position
y1 = 0   # y limits of a line
y2 = 80
plt.plot((x1,x1),(y1,y2),'--')
plt.plot((x2,x2),(y1,y2),'--')
plt.savefig("gantt chart.png",dpi=800)        
        
###############################################################################
'''Buffer profile '''
###############################################################################




